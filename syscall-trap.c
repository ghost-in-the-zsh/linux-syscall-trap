/*
 * (c) 2015 Raymond L. Rivera
 * CSc-239: Adv. Operating Systems
 * Prof. J. Ouyang
 * Fall 2015
 */

#include "syscall-trap.h"


/*
 * Module startup/cleanup functions
 */
static int __init systrap_startup(void)
{
	pr_debug("systrap: setting up module\n");
	pr_debug("systrap: tracked user id: %d\n", uid);

	if(systrap_create_procfs_entries() != 0)
		goto cleanup;

	systrap_replace_syscalls();

	pr_debug("systrap: module setup complete\n");
	return 0;

cleanup:
	systrap_restore_syscalls();
	systrap_remove_procfs_entries();
	return -EIO;
}

static void __exit systrap_cleanup(void)
{
	pr_debug("systrap: cleaning up module\n");

	/* undo operations in the opposite order of __init */
	systrap_restore_syscalls();
	systrap_remove_procfs_entries();

	pr_debug("systrap: module cleanup complete\n");
}


/*
 * System call overrides
 */
static asmlinkage ssize_t systrap_read(unsigned int fd, char __user *buf, size_t count)
{
	/*
	 * cred->uid is of type struct kuid_t
	 */
	if(current->cred->uid.val == (uid_t) uid) {
		struct histlog_entry log = {
			.opname  = "read",
			.bytecnt = count
		};
		if(systrap_get_filename(fd, log.filepath) == 0) {
			int i;
			for(i = 0; dir_filters[i] != NULL; ++i) {
				if(strstr(log.filepath, dir_filters[i]) != NULL) {
					systrap_log_history(&log);
					pr_debug("systrap: %s is reading %s\n", log.username, log.filepath);
					stats.read_cnt++;
					break;
				}
			}
		}
	}
	return original_read(fd, buf, count);
}

static asmlinkage ssize_t systrap_write(unsigned int fd, const char __user *buf, size_t count)
{
	if(current->cred->uid.val == (uid_t) uid) {
		struct histlog_entry log = {
			.opname  = "write",
			.bytecnt = count
		};
		if(systrap_get_filename(fd, log.filepath) == 0) {
			int i;
			for(i = 0; dir_filters[i] != NULL; ++i) {
				if(strstr(log.filepath, dir_filters[i]) != NULL) {
					systrap_log_history(&log);
					pr_debug("systrap: %s is writing %s\n", log.username, log.filepath);
					stats.write_cnt++;
					break;
				}
			}
		}
	}
	return original_write(fd, buf, count);
}

static asmlinkage ssize_t systrap_open(const char __user *filename, int flags, umode_t mode)
{
	if(current->cred->uid.val == (uid_t) uid) {
		int i;
		for(i = 0; dir_filters[i] != NULL; ++i) {
			if(strstr(filename, dir_filters[i]) != NULL) {
				struct histlog_entry log = {
					.opname   = "open",
					.bytecnt  = 0
				};
				strncpy(log.filepath, filename, FILEPATH_LENGTH);
				systrap_log_history(&log);
				pr_debug("systrap: %s is opening %s\n", log.username, log.filepath);
				stats.open_cnt++;
				break;
			}
		}
	}
	return original_open(filename, flags, mode);
}

static asmlinkage ssize_t systrap_close(unsigned int fd)
{
	if(current->cred->uid.val == (uid_t) uid) {
		struct histlog_entry log = {
			.opname  = "close",
			.bytecnt = 0
		};
		if(systrap_get_filename(fd, log.filepath) == 0) {
			int i;
			for(i = 0; dir_filters[i] != NULL; ++i) {
				if(strstr(log.filepath, dir_filters[i]) != NULL) {
					systrap_log_history(&log);
					pr_debug("systrap: %s is closing %s\n", log.username, log.filepath);
					stats.close_cnt++;
					break;
				}
			}
		}
	}
	return original_close(fd);
}


/*
 * Pseudo-file system utilities.
 */
static int systrap_create_procfs_entries(void)
{
	pr_debug("systrap: creating procfs entries\n");

	stdir = proc_mkdir(PROCFS_PARENT_DIR, NULL);
	if(!stdir)
		return -EIO;

	ststat = proc_create(PROCFS_STATS_FILE, S_IWUSR | S_IRUGO, NULL, &st_stat_fops);
	if(!ststat)
		return -EIO;

	stlog = proc_create(PROCFS_HISTORY_FILE, S_IWUSR | S_IRUGO, NULL, &st_log_fops);
	if(!stlog)
		return -EIO;

	return 0;
}

static void systrap_remove_procfs_entries(void)
{
	pr_debug("systrap: removing procfs entries\n");
	if(ststat)
		proc_remove(ststat);

	if(stlog)
		proc_remove(stlog);

	if(stdir)
		proc_remove(stdir);
}

static int systrap_procfs_diskopen(struct inode *inode, struct file *file)
{
	return single_open(file, systrap_procfs_diskprint, NULL);
}

static int systrap_procfs_diskprint(struct seq_file *sf, void *ptr)
{
	seq_printf(sf, "%s: %d\n"  , "UserID    ", uid);
	seq_printf(sf, "%s: %llu\n", "ReadTotal ", stats.read_cnt);
	seq_printf(sf, "%s: %llu\n", "WriteTotal", stats.write_cnt);
	seq_printf(sf, "%s: %llu\n", "OpenTotal ", stats.open_cnt);
	seq_printf(sf, "%s: %llu\n", "CloseTotal", stats.close_cnt);
	return 0;
}

static int systrap_procfs_logopen(struct inode *inode, struct file *file)
{
	return single_open(file, systrap_procfs_logprint, NULL);
}

static int systrap_procfs_logprint(struct seq_file *sf, void *ptr)
{
	seq_printf(sf, "%s\n", HISTORY_HEADER_LINE);
	if(strcmp(history_log, "") != 0) {
		/* don't add newline on print; each line should have one */
		seq_printf(sf, "%s", history_log);
		/* log consumed; make room for future data */
		strcpy(history_log, "");
	}
	return 0;
}


/*
 * Replace/Restore utilities for system calls.
 */
static void systrap_replace_syscalls(void)
{
	pr_debug("systrap: replacing system table entries\n");
	pr_debug("systrap: sys_read  table index   : %u\n", __NR_read);
	pr_debug("systrap: sys_write table index   : %u\n", __NR_write);
	pr_debug("systrap: sys_open  table index   : %u\n", __NR_open);
	pr_debug("systrap: sys_close table index   : %u\n", __NR_close);
	pr_debug("systrap: sys_readlink table index: %u\n", __NR_readlink);
	pr_debug("systrap: sys_unlink table index  : %u\n", __NR_unlink);

	/**
	 * The system call table is in a read-only page, so we read the
	 * current value of the control register cr0 and remove the write
	 * protect (WP) flag on bit 16 (0-based) to allow writing to read-only
	 * pages. See https://en.wikipedia.org/wiki/Control_register#CR0
	 */
	write_cr0(read_cr0() & ~0x10000);	/* ~0x10000 = 0xfffeffff */

	original_read     = (void *) syscall_table[__NR_read];
	original_write    = (void *) syscall_table[__NR_write];
	original_open     = (void *) syscall_table[__NR_open];
	original_close    = (void *) syscall_table[__NR_close];

	syscall_table[__NR_read]  = (unsigned long) systrap_read;
	syscall_table[__NR_write] = (unsigned long) systrap_write;
	syscall_table[__NR_open]  = (unsigned long) systrap_open;
	syscall_table[__NR_close] = (unsigned long) systrap_close;

	/**
	 * Re-enable the read-only protection for read-only pages.
	 */
	write_cr0(read_cr0() | 0x10000);
	pr_debug("systrap: system table entries replaced\n");

	/*
	 * Only take the address; don't need to replace/restore
	 */
	original_readlink = (void *) syscall_table[__NR_readlink];
	original_unlink   = (void *) syscall_table[__NR_unlink];
}

static void systrap_restore_syscalls(void)
{
	pr_debug("systrap: restoring system table entries\n");
	write_cr0(read_cr0() & ~0x10000);

	/* make sure no other modules have made changes before restoring */
	if(syscall_table[__NR_read] == (unsigned long) systrap_read)
	{
		syscall_table[__NR_read] = (unsigned long) original_read;
		pr_debug("systrap: restored sys_read\n");
	}
	else
	{
		pr_warn("systrap: sys_read not restored; different address\n");
	}

	if(syscall_table[__NR_write] == (unsigned long) systrap_write)
	{
		syscall_table[__NR_write] = (unsigned long) original_write;
		pr_debug("systrap: restored sys_write\n");
	}
	else
	{
		pr_warn("systrap: sys_write not restored; different address\n");
	}

	if(syscall_table[__NR_open] == (unsigned long) systrap_open)
	{
		syscall_table[__NR_open] = (unsigned long) original_open;
		pr_debug("systrap: restored sys_open\n");
	}
	else
	{
		pr_warn("systrap: sys_open not restored; different address\n");
	}

	if(syscall_table[__NR_close] == (unsigned long) systrap_close)
	{
		syscall_table[__NR_close] = (unsigned long) original_close;
		pr_debug("systrap: restored sys_close\n");
	}
	else
	{
		pr_warn("systrap: sys_close not restored; different address\n");
	}

	write_cr0(read_cr0() | 0x10000);
	pr_debug("systrap: original system table entries restored\n");
}


/*
 * General utility functions
 */
static int systrap_get_filename(unsigned int fd, char fname[])
{
	int bytes;
	mm_segment_t old_fs = get_fs();

	char *linkpath = (char *)kmalloc(FILEPATH_LENGTH, GFP_KERNEL);
	if(!linkpath) {
		pr_err("systrap: error allocating %lu bytes for link path\n", (size_t) FILEPATH_LENGTH);
		return -ENOMEM;
	}

	/* verify snprintf did not truncate the string or fail outright */
	bytes = snprintf(linkpath, FILEPATH_LENGTH, "/proc/%u/fd/%u", current->pid, fd);
	if(bytes < 0 || bytes >= FILEPATH_LENGTH) {
		pr_err("systrap: [%d] error building link path for file name\n", bytes);
		goto cleanup;
	}

	/*
	 * Normally, readlink expects to receive/return arguments
	 * from/to user space buffers. However, buffers here are all
	 * in kernel space, so we need to switch to kernel space mode
	 * in order for readlink to not generate a -EFAULT code.
	 *
	 * It seems fs/gs segment registers are given their purpose by the
	 * OS itself, unlike general registers like eax, ebx, etc. This means
	 * Windows and GNU/Linux use them differently. Windows uses them to
	 * hold the base address of Thread Info Blocks (TIBs), and Linux to
	 * point to Kernel Processor Control Region (KPCR).
	 *
	 * See:
	 * 	https://stackoverflow.com/a/10810340/4594973
	 * 	https://www.microsoft.com/msj/archive/s2ce.aspx
	 * 	https://stackoverflow.com/a/4860301/4594973
	 */
	set_fs(KERNEL_DS);
	bytes = original_readlink(linkpath, fname, FILEPATH_LENGTH);
	set_fs(old_fs);

	if(bytes == -1 || bytes + 1 >= FILEPATH_LENGTH) {
		pr_err("systrap: [%d] error reading path from link %s\n", bytes, linkpath);
		goto cleanup;
	}

	/* manually append a null char; readlink does NOT do this */
	fname[bytes + 1] = '\0';
	return 0;

cleanup:
	if(linkpath)
		kfree(linkpath);
	return -EINVAL;
}

static int systrap_get_username(char username[])
{
	char path[FILEPATH_LENGTH] = {};

	/* allocate dynamically to avoid stack frame warnings beyond 1024 bytes */
	char *read_buff  = NULL;

	/* tokens use to parse username from environment */
	char *token1;
	char *token2;

	int i, fd, statcode = 0;
	ssize_t byte_cnt;
	mm_segment_t old_fs = get_fs();

	read_buff = (char *)kmalloc(READ_BUFFER_SIZE, GFP_ATOMIC);
	if(!read_buff) {
		pr_err("systrap: error allocating %u bytes for read buffer\n", READ_BUFFER_SIZE);
		statcode = -ENOMEM;
		goto cleanup;
	}

	/* verify snprintf did not truncate the string or fail outright */
	byte_cnt = snprintf(path, FILEPATH_LENGTH, "/proc/%u/environ", current->pid);
	if(byte_cnt < 0 || byte_cnt >= FILEPATH_LENGTH) {
		pr_err("systrap: [%ld] error building link path for user name\n", byte_cnt);
		statcode = -EINVAL;
		goto cleanup;
	}

	set_fs(KERNEL_DS);
	fd = original_open(path, O_RDONLY | O_LARGEFILE, 0400);
	if(fd < 0) {
		pr_err("systrap: error getting username while opening %s\n", path);
		statcode = -ENOENT;
		goto cleanup;
	}

	if((byte_cnt = original_read(fd, read_buff, READ_BUFFER_SIZE)) < 0) {
		pr_err("systrap: error getting username while reading %s\n", path);
		statcode = -EIO;
		goto cleanup;
	}
	original_close(fd);

	/*
	 * environ file lines are delimted by null chars and they need to
	 * be replaced to allow strstr function to find our substring token
	 * later without getting stuck on the 1st line
	 */
	for(i = 0; i < byte_cnt; ++i)
		if(read_buff[i] == '\0')
			read_buff[i] = ' ';
	read_buff[byte_cnt-1] = '\0';

	/*
	 * Find the starting location for the token using our copy to make sure
	 * our token points into the copy to be modified later.
	 */
	token1 = strstr(read_buff, "USER=");
	if(!token1) {
		pr_debug("systrap: error searching for 'USER' token in %s\n", path);
		token1 = strstr(read_buff, "USERNAME=");
		if(!token1) {
			pr_err("systrap: error searching for 'USERNAME' token in %s\n", path);
			statcode = -EINVAL;
			goto cleanup;
		}
	}

	/*
	 * The strsep function moves the 1st arg one position after the delimiter
	 * and replaces the delimiter in the original string with a null char, while
	 * returning the starting point of the string before the delimiter was found.
	 *
	 * With this, we first position ourselves at the "USER=<name>\0" substring, then
	 * break it up again on '=', leaving token1 with the username. Since the string
	 * is now null-terminated, we can safely use strcpy instead of strncpy with an
	 * arbitrary number of bytes for the username.
	 */
	token1 = strsep(&token1, " ");	/* token1 = "USER=ray\0" */
	token2 = strsep(&token1, "=");	/* token2 = "USER\0"; token1 = "ray\0" */
	strcpy(username, token1);	/* username = "ray\0" */

cleanup:
	if(read_buff)
		kfree(read_buff);
	set_fs(old_fs);
	return statcode;
}

static int systrap_get_cmdline(char cmdline[])
{
	char path[FILEPATH_LENGTH]   = {};
	char buff[WRITE_BUFFER_SIZE] = {};
	int fd, error = 0;
	mm_segment_t old_fs = get_fs();

	/* verify snprintf did not truncate the string or fail outright */
	ssize_t byte_cnt = snprintf(path, FILEPATH_LENGTH, "/proc/%u/cmdline", current->pid);
	if(byte_cnt < 0 || byte_cnt >= FILEPATH_LENGTH) {
		pr_err("systrap: [%ld] error building link path for command line\n", byte_cnt);
		error = -EINVAL;
		goto cleanup;
	}

	set_fs(KERNEL_DS);
	fd = original_open(path, O_RDONLY | O_LARGEFILE, 0400);
	if(fd < 0) {
		pr_err("systrap: error getting command line while opening %s\n", path);
		error = -ENOENT;
		goto cleanup;
	}

	if((byte_cnt = original_read(fd, buff, WRITE_BUFFER_SIZE)) < 0) {
		pr_err("systrap: error getting command line while reading %s\n", path);
		error = -EIO;
		goto cleanup;
	}
	original_close(fd);

	strcpy(cmdline, buff);

cleanup:
	set_fs(old_fs);
	return error;
}

static int systrap_log_history(struct histlog_entry *entry)
{
	char line[WRITE_BUFFER_SIZE] = {};
	size_t linelen;
	size_t loglen;
	ssize_t bytes;

	do_gettimeofday(&entry->time);
	systrap_get_username(entry->username);
	systrap_get_cmdline(entry->cmdline);

	bytes = snprintf(line, WRITE_BUFFER_SIZE, HISTORY_ENTRY_FORMAT,
			entry->time.tv_sec,
			entry->username,
			entry->opname,
			entry->bytecnt,
			entry->cmdline,
			entry->filepath
		);
	if(bytes < 0 || bytes >= FILEPATH_LENGTH) {
		pr_err("systrap: [%ld] not enough space for log line buffer\n", bytes);
		return -ENOMEM;
	}

	/* strlen does not count null byte */
	linelen = strlen(line) + 1;
	loglen  = strlen(history_log) + 1;

	/* do we have enough space for the line? */
	if(loglen + linelen > HISTORY_BUFFER_SIZE) {
		pr_warn("systrap: skipping line on full history log");
		return -ENOMEM;
	}

	/* these write linelen chars + 1 null byte to the buffer */
	if(strcmp(history_log, "") == 0)
		strncpy(history_log, line, linelen);
	else
		strncat(history_log, line, linelen);

	/* safety first */
	history_log[loglen + linelen] = '\0';
	return 0;
}
