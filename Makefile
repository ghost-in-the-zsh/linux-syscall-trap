obj-m	+= syscall-trap.o

# kernel build and installation directories
KDIR	+= /lib/modules/$(shell uname -r)/build
IDIR	+= /lib/modules/$(shell uname -r)/extra

all:
	$(MAKE) -C $(KDIR) SUBDIRS=$(shell pwd) modules

install:
	$(MAKE) -C $(KDIR) SUBDIRS=$(shell pwd) modules_install

uninstall:
	rm $(IDIR)/syscall-trap.ko

clean:
	rm -rf *.o *.ko *.mod.* .*.cmd *.symvers *.order .tmp_versions/
