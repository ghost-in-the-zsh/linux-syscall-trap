# Linux I/O System Call Monitoring Module
Implement a Linux kernel module to intercept system calls that attempt to
network activity and file I/O and record some information.

## Required Features
The module must be able to do the following:

1. Intercept and log the file I/O system calls, i.e. `open()`, `read()`, and `write()`, into a separate file.
2. Intercept and log network activities into a separate file.
3. Generate results that are similar to the results template provided as an example.

The following data must be included in the file I/O results:

1. timestamp
2. username
3. cmdline
4. operation
5. filename
6. file descriptor
7. offset
8. data_size
9. etc...?

## Optional Features
Add the following functionality to the module:

1. A filter so that it will only log the specified directories.
2. Writing the traces to a pseudo file under "/proc", instead of a regular file.

# Eclipse Configuration
The following were helpful in configuring the IDE to work on kernel module development.

* [HowTo use the CDT to navigate Linux kernel source](https://wiki.eclipse.org/HowTo_use_the_CDT_to_navigate_Linux_kernel_source)
* [How to compile hello world linux kernel module in Eclipse CTD?](https://stackoverflow.com/questions/18974878/how-to-compile-hello-world-linux-kernel-module-in-eclipse-cdt)
* [Configuring Eclipse for Linux Kernel module development](https://stackoverflow.com/questions/22168095/configuring-eclipse-for-linux-kernel-module-development)

## Additional Resources
[Linux Kernel Development Second Edition](http://www.makelinux.net/books/lkd2/)

# Module Installation Path
When the `make install` rule is run, the `.ko` file will be installed at `/lib/modules/$(uname -r)/extra/<module_name>.ko`. The `make uninstall` rule in the Makefile already takes care of removing it. The rule will need to be updated if the module's name changes.

# Usage

When loading the module, the ID of a specific user must be provided. For example:

    insmod syscall-trap.ko uid=1000

This allows the module to 'filter' by user. If one is not provided, then it defaults to `0`, the root user.
You might want to modify the directory filter paths before building the module with the `make` command.
