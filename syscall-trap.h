/*
 * (c) 2015 Raymond L. Rivera
 * CSc-239: Adv. Operating Systems
 * Prof. J. Ouyang
 * Fall 2015
 */

#define DEBUG

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/errno.h>		/* error codes (e.g. EIO) */
#include <linux/proc_fs.h>		/* proc_dir_entry struct, etc. */
#include <linux/seq_file.h>		/* seq_read, etc. */
#include <linux/sched.h>		/* need access to current task to get uid */
#include <linux/slab.h>			/* kmalloc */
#include <asm-generic/current.h>	/* current/get_current() macros */
#include <asm-generic/uaccess.h>	/* get_fs, set_fs, KERNEL_DS */


MODULE_AUTHOR("Raymond L. Rivera");
MODULE_LICENSE("GPL");			/* GPL v2 or later */
MODULE_DESCRIPTION("I/O system call interception hackery :)");

/*
 * Generic values
 */
#define FILEPATH_LENGTH		(255+1)
#define USERNAME_LENGTH		(31+1)
#define SYSOPNAME_LENGTH	(15+1)
#define CMDLINE_LENGTH		(63+1)
#define READ_BUFFER_SIZE	(2047+1)
#define WRITE_BUFFER_SIZE	(511+1)
#define HISTORY_BUFFER_SIZE	(16383+1)	/* 16KB log */
#define HISTORY_HEADER_LINE	"time_secs,username,operation,byte_cnt,cmdline,filename"
#define HISTORY_ENTRY_FORMAT	"%ld,%s,%s,%lu,%s,%s\n"


/*
 * Directory structure under /proc/
 */
#define PROCFS_PARENT_DIR	"syscall_trap"
#define PROCFS_STATS_FILE	PROCFS_PARENT_DIR "/stats"
#define PROCFS_HISTORY_FILE	PROCFS_PARENT_DIR "/history"


/*
 * Module startup/cleanup functions
 */
static int  __init systrap_startup(void);
static void __exit systrap_cleanup(void);

module_init(systrap_startup);
module_exit(systrap_cleanup);


/*
 * General functions
 */
static int systrap_create_procfs_entries(void);
static void systrap_remove_procfs_entries(void);
static void systrap_replace_syscalls(void);
static void systrap_restore_syscalls(void);


/*
 * Helper functions to open/read proc-fs pseudo-files
 */
static int systrap_procfs_diskopen(struct inode *inode, struct file *file);
static int systrap_procfs_diskprint(struct seq_file *sf, void *ptr);
static int systrap_procfs_logopen(struct inode *inode, struct file *file);
static int systrap_procfs_logprint(struct seq_file *sf, void *ptr);


/*
 * Structure definitions
 */
static const struct file_operations st_stat_fops = {
	.owner      = THIS_MODULE,
	.open       = systrap_procfs_diskopen,
	.read       = seq_read,
	.llseek     = seq_lseek,
	.release    = single_release,
};

static const struct file_operations st_log_fops = {
	.owner      = THIS_MODULE,
	.open       = systrap_procfs_logopen,
	.read       = seq_read,
	.llseek     = seq_lseek,
	.release    = single_release,
};


/*
 * Gather system call usage statistics; displayed under /proc
 */
struct io_stats {
	unsigned long long read_cnt;
	unsigned long long write_cnt;
	unsigned long long open_cnt;
	unsigned long long close_cnt;
};

static struct io_stats stats = {
	.read_cnt  = 0,
	.write_cnt = 0,
	.open_cnt  = 0,
	.close_cnt = 0,
};

struct histlog_entry {
	struct timeval time;
	char username[USERNAME_LENGTH];
	char opname[SYSOPNAME_LENGTH];
	char filepath[FILEPATH_LENGTH];
	char cmdline[CMDLINE_LENGTH];
	size_t bytecnt;
};


/*
 * Objects for hierarchical pseudo-file system structure.
 */
static struct proc_dir_entry *stdir  = NULL;	/* parent directory */
static struct proc_dir_entry *ststat = NULL;	/* stats file */
static struct proc_dir_entry *stlog  = NULL;	/* history log file */


/*
 * Address is from /boot/System.map-4.2.0-19-generic, but can change
 * between kernel versions.
 *
 * The map entry has the following format:
 * 	<address> <type> <symbol>
 *
 * For example:
 * 	ffffffff818001c0 R sys_call_table
 *
 * For information on types, see https://en.wikipedia.org/wiki/System.map#Symbol_types
 */
static unsigned long *syscall_table = (unsigned long *) 0xffffffff818001c0;

/*
 * Buffer to keep some historic data about system use.
 */
static char history_log[HISTORY_BUFFER_SIZE] = {};

/*
 * Pay attention only to files under the following directory/ies
 */
static char *dir_filters[] = {
	"/home/ray/",
	NULL
};


/*
 * Named parameters for module. The usage format is:
 * 	insmod <module-name>.ko <param-name1>=<value1> <param-name2>=<value2> ...
 *
 * E.g.
 * 	insmod syscall-trap.ko uid=1000
 *
 * Parameter format:
 * 	module_param(<variable>, <type>, <permissions>);
 */
static int uid;
module_param(uid, int, 0);
MODULE_PARM_DESC(uid, "The ID of the user we want to track.");


/*
 * Function pointers for original system calls
 */
static asmlinkage ssize_t (*original_read)(unsigned int fd, char __user *buf, size_t count);
static asmlinkage ssize_t (*original_write)(unsigned int fd, const char __user *buf, size_t count);
static asmlinkage ssize_t (*original_open)(const char __user *filename, int flags, umode_t mode);
static asmlinkage ssize_t (*original_close)(unsigned int fd);
static asmlinkage ssize_t (*original_readlink)(const char *pathname, char __user *buf, size_t bufsize);
static asmlinkage ssize_t (*original_unlink)(const char __user *pathname);


/*
 * General utility functions
 */
static int systrap_get_filename(unsigned int fd, char fname[]);
static int systrap_get_username(char username[]);
static int systrap_get_cmdline(char cmdline[]);
static int systrap_log_history(struct histlog_entry *entry);
